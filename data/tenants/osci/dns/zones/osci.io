$TTL 1D
@				IN SOA		ns1.osci.io. hostmaster.osci.io. (
						2018072000	; serial (YYYYMMDDRR)
						3600		; Refresh
						1800		; Retry
						604800		; Timeout
						86400 )		; Negative answer TTL
				IN NS		ns1
				IN NS		ns2
                                IN NS           ns1.redhat.com.
                                IN NS           ns2.redhat.com.
                                IN NS           ns3.redhat.com.
                                IN NS           ns4.redhat.com.
				IN MX		10	mx1
				IN MX		20	mx2
                                IN A            8.43.85.237
; DigitCert
                                IN TXT		"619488  domainadmin@redhat.com"

; physical machines
speedy				IN A		8.43.85.225
guido				IN A		8.43.85.226
seymour                         IN A            8.43.85.208
jerry                           IN A            8.43.85.207

; basic domain services
polly				IN A		8.43.85.229
francine			IN A		8.43.85.230
ns1				IN A		8.43.85.229
ns2				IN A		8.43.85.230
; keep me A/AAAA RR please, CNAME would break mails
mx1				IN A		8.43.85.229
mx2				IN A 		8.43.85.230
mail				IN CNAME	polly
ntp1				IN CNAME	speedy
ntp2				IN CNAME	guido
catton                          IN A            8.43.85.192

soeru                           IN A            8.43.85.211

; websites
tickets				IN A		8.43.85.231
www                             IN A            8.43.85.237
pxe                             IN CNAME        www
sup                             IN CNAME        catton
openjdk-sources                 IN A            8.43.85.238
pulp-repo                       IN A            8.43.85.195
pulp-www                        IN A            8.43.85.236
scl-web                         IN A            8.43.85.196
spice-web                       IN A            8.43.85.198
tracker                         IN CNAME        piwik-vm.osci.io.
piwik-vm                        IN A            8.43.85.206
asciibinder-web                 IN A            8.43.85.227
; jd's builder
gnocchi                         IN A            8.43.85.232

; Fedora Atomic
fedora-atomic-1                 IN A            8.43.85.200
fedora-atomic-2                 IN A            8.43.85.201
fedora-atomic-3                 IN A            8.43.85.202
fedora-atomic-4                 IN A            8.43.85.203
fedora-atomic-5                 IN A            8.43.85.204
fedora-atomic-6                 IN A            8.43.85.205

pcp                             IN A            8.43.85.210

gdb-buildbot                    IN A            8.43.85.197

patternfly-forum                IN A            8.43.85.214

openjdk-aarch32                 IN A            51.15.204.18
; Management VLAN
$ORIGIN adm.osci.io.

speedy                          IN A            172.24.31.1
cmm-catatonic                   IN A            172.24.31.4
catton                          IN A            172.24.31.9
sup                             IN CNAME        catton
switch-a1-catatonic             IN A            172.24.31.247
switch-a2-catatonic             IN A            172.24.31.248
conserve                        IN A            172.24.31.249


; Internal VLAN
$ORIGIN int.osci.io.

speedy                          IN A            172.24.32.1
guido                           IN A            172.24.32.2
seymour                         IN A            172.24.32.5
jerry                           IN A            172.24.32.6
raagu                           IN A            172.24.32.7
catton                          IN A            172.24.32.9
sup                             IN CNAME        catton
tempusfugit			IN A		172.24.32.10
osci-web-builder                IN A            172.24.32.11
pulp-web-builder                IN A            172.24.32.12
osas-community-web-builder	IN A		172.24.32.14
ovirt-web-builder		IN A		172.24.32.15
rdo-web-builder                 IN A            172.24.32.16
asciibinder-web-builder         IN A            172.24.32.17

; Services VLAN
$ORIGIN srv.osci.io.

speedy                          IN A            172.24.33.1
guido                           IN A            172.24.33.2
seymour                         IN A            172.24.33.5
jerry                           IN A            172.24.33.6
lucille                         IN A            172.24.33.8
catton                          IN A            172.24.33.9
sup                             IN CNAME        catton
gdb-buildbot                    IN A            172.24.33.10

