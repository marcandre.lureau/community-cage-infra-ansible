---

- hosts: speedy.osci.io
  tasks:
    - name: Ensure we have the needed facts about our jump host
      setup:

# TODO: manage SMCIPMITool installation
- hosts: supermicro_microblade_blades
  gather_facts: False
  # the CMM is not happy with too many connections
  serial: 3
  vars:
    cmm: "{{ hostvars['cmm-catatonic.adm.osci.io'] }}"
    cmm_command: "SMCIPMITool {{ cmm.ansible_host }} {{ cmm.chassis_module.ipmi.user }} {{ cmm.chassis_module.ipmi.password }}"
    # set this environment variable to update the IPMI admin passwd for all (defined) blades
    old_ipmi_administrator_password: "{{ lookup('env', 'OLD_IPMI_ADM_PWD') }}"
    ipmi_command: "SMCIPMITool {{ blade.ipmi.ip }} {{ blade.ipmi.administrator.user }} {{ (old_ipmi_administrator_password != '') | ternary(old_ipmi_administrator_password, blade.ipmi.administrator.password) }}"
    # we only have blades with a single node, so the following has been simplified
    node_id: 1
    # IPMI accounts
    administrator_user_id: 2
    operator_user_id: 3
  tasks:
    - name: "Setup IPMI Configuration for blade {{ blade.pos }}"
      block:
        # a new name does not always show up until the blade is rebooted
        - name: Set Blade Name
          command: "{{ cmm_command }} microblade node name {{ blade.pos }} {{ node_id }} {{ inventory_hostname_short }}"
        - name: Disable IPMI DHCP
          command: "{{ cmm_command }} microblade node dhcp {{ blade.pos }} {{ node_id }} 1"
        - name: Set IPMI IP
          command: "{{ cmm_command }} microblade node ip {{ blade.pos }} {{ node_id }} {{ blade.ipmi.ip }}"
        - name: Set IPMI Operator Account
          command: "{{ ipmi_command }} user add {{ operator_user_id }} {{ blade.ipmi.operator.user }} {{ blade.ipmi.operator.password }} 3"
          when: blade.ipmi.operator.password is defined
        - name: Remove IPMI Operator Account
          command: "{{ ipmi_command }} user delete {{ operator_user_id }}"
          when: blade.ipmi.operator.password is not defined
        # do this last to avoid affecting other commands
        - name: Set IPMI Administrator Account
          command: "{{ ipmi_command }} user add {{ administrator_user_id }} {{ blade.ipmi.administrator.user }} {{ blade.ipmi.administrator.password }} 4"
          when: old_ipmi_administrator_password != ''
      delegate_to: speedy.osci.io
  tags: cmm_blade_setup

- hosts: supermicro_microblade_sw
  gather_facts: False
  vars:
    tftp_dir: /var/lib/tftpboot
    subnet_prefix: "{{ cage_vlans['OSAS-Management'].subnet | ipaddr('prefix') }}"
  tasks:
    - name: Fetch facts for NTP servers
      setup:
      delegate_to: "{{ item }}"
      delegate_facts: True
      when: hostvars[item].ansible_vlan431 is not defined
      with_items: "{{ groups['ntp_servers'] }}"
    - name: Create temporary file for final configuration
      tempfile:
        suffix: temp
      register: config_file
      # we only care about the final file changed status
      changed_when: False
    - name: Generate Network Configuration for Supermicro SW
      include_role:
        name: supermicro_microblade_sw
      vars:
        contact_email: "{{ osas_root_email }}"
        mgmt_ip: "{{ ansible_host }}/{{ subnet_prefix }}"
        ntp: "{{ groups['ntp_servers'] | map('extract', hostvars) | map(attribute='ansible_vlan431') | map(attribute='ipv4') | map(attribute='address') | sort | list }}"
        vlans: "{{ cage_vlans }}"
        blades_group: supermicro_microblade_blades
        output: "{{ config_file.path }}"
    - name: Transfer final configuration to PXE host
      copy:
        src: "{{ config_file.path }}"
        dest: "{{ tftp_dir }}/{{ inventory_hostname }}.conf"
      delegate_to: speedy.osci.io
    - name: Remove temporary file for final configuration
      file:
        path: "{{ config_file.path }}"
        state: absent
      # we only care about the final file changed status
      changed_when: False
  tags: sw_conf_gen

- name: Load Network Configuration into Supermicro SW
  hosts: supermicro_microblade_sw
  gather_facts: False
  serial: 1
  vars:
    tftp_ip: "{{ hostvars['speedy.osci.io'].ansible_vlan431.ipv4.address }}"
    cmm: "{{ hostvars['cmm-catatonic.adm.osci.io'] }}"
    cmm_command: "SMCIPMITool {{ cmm.ansible_host }} {{ cmm.chassis_module.ipmi.user }} {{ cmm.chassis_module.ipmi.password }}"
  tasks:
    - name: Load Generated Configuration on the Device
      ios_command:
        commands: "copy tftp://{{ tftp_ip }}/{{ inventory_hostname }}.conf startup-config"
    - name: Reboot Supermicro SW
      command: "{{ cmm_command }} microblade switch power {{ chassis_module.pos }} Reset"
      changed_when: True
      delegate_to: speedy.osci.io
      tags: sw_reboot
    - name: Wait for device to show up
      wait_for:
        port: 22
        host: '{{ inventory_hostname }}'
        delay: 10
        timeout: 600
      delegate_to: speedy.osci.io
  tags: sw_conf_apply

