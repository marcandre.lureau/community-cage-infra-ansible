# Community Cage Infrastructure Management using Ansible

## Introduction

We offer a range of full-lifecycle services for open source community projects that are strategic to Red Hat. Services range from managed hosting to server colocation (ping/power/pipe/racks) in multiple data centers. We also provide project planning & execution, budget planning, and purchasing support for tenants.

This is a physical co-location, managed + virtual services including private cloud, and public cloud offering from the Open Source and Standards (OSAS) working with Regional IT, ACS, and PnT DevOps.

This repository contains Ansible rules to manage shared services and VM offering for all tenants of the cage.

You need Ansible >=2.3 to be able to handle the (new) YAML-based 'hosts' file format.

## Admin-specific Production Settings

You can use `group_vars/all/local_settings.yml` for you local
settings like `ansible_become_pass` if your computer storage is
encrypted. Use `--ask-sudo-pass` if you don't want to use this
method. Currently Ansible is unable to ask _when needed_ so
the global setting has been disabled in `ansible.cfg`.

## Dealing with Secrets

We use Ansible Vault (`ansible-vault` command) to hide some parameters
like service credentials or emails to avoid SPAM.

To make it easy all such files are named '\*.vault.yml' and git
attributes are defined to make diff-ing and merging easy.

Your config needs to be enhanced to tell git how to handle these files.
This is very easy, look at this URL for more info:
  https://github.com/building5/ansible-vault-tools

