---
# Group assignation for hosts:
# - tenant group (necessary, only one)
# - 'unmanaged' group, if provisonned by us but managed by the tenant
# - if not unmanaged:
#   + service groups
#   + specific groups
#
all:
  vars:
    ansible_user: root
    kakunin_os_image: "centos/7"

  children:
    tenant_osci:
      hosts:
        speedy.osci.io:
        guido.osci.io:
        seymour.osci.io:
        jerry.osci.io:
        osas-community-web-builder.int.osci.io:
          ansible_host: 172.24.32.14
        polly.osci.io:
          ansible_host: 8.43.85.229
        francine.osci.io:
          ansible_host: 8.43.85.230
        tickets.osci.io:
          ansible_host: 8.43.85.231
          ansible_python_interpreter: /usr/bin/python3
        osci-web-builder.int.osci.io:
          ansible_host: 172.24.32.11
          ansible_python_interpreter: /usr/bin/python3
        www.osci.io:
          ansible_host: 8.43.85.237
        piwik-vm.osci.io:
          ansible_host: 8.43.85.206
          ansible_python_interpreter: /usr/bin/python3
        soeru.osci.io:
          ansible_host: 8.43.85.211
        lucille.srv.osci.io:
        catton.osci.io:
          ansible_python_interpreter: /usr/bin/python3

    tenant_asciibinder:
      hosts:
        asciibinder-web.osci.io:
          ansible_host: 8.43.85.227
        asciibinder-web-builder.int.osci.io:
          ansible_host: 172.24.32.17

    tenant_gdb:
      hosts:
        gdb-buildbot.osci.io:
          ansible_host: 8.43.85.197

    tenant_gnocchi:
      hosts:
        gnocchi.osci.io:
          ansible_host: 8.43.85.232

    tenant_minishift:
      hosts:
        lists.minishift.io:
          ansible_host: 8.43.85.234

    tenant_nfs_ganesha:
      hosts:
        lists.nfs-ganesha.org:
          ansible_host: 8.43.85.212

    tenant_openjdk:
      hosts:
        openjdk-sources.osci.io:
          ansible_host: 8.43.85.238

    tenant_opensourceinfra:
      hosts:
        lists.opensourceinfra.org:
          ansible_host: 8.43.85.233

    tenant_ovirt:
      hosts:
        ovirt-web-builder.int.osci.io:
          ansible_host: 172.24.32.15
        mail.ovirt.org:
          ansible_host: 8.43.85.194
        monitoring.ovirt.org:
          ansible_host: 8.43.85.199
        www.ovirt.org:
          ansible_host: 8.43.85.224

    tenant_pcp:
      hosts:
        pcp.osci.io:
          ansible_host: 8.43.85.210

    tenant_po4a:
      hosts:
        www.po4a.org:
          ansible_host: 8.43.85.209
        lists.po4a.org:
          ansible_host: 8.43.85.228

    tenant_pulp:
      hosts:
        pulp-repo.osci.io:
          ansible_host: 8.43.85.195
        www.pulpproject.org:
          ansible_host: 8.43.85.236
        pulp-web-builder.int.osci.io:
          ansible_host: 172.24.32.12
          ansible_python_interpreter: /usr/bin/python3

    tenant_rdo:
      hosts:
        rdo-web-builder.int.osci.io:
          ansible_host: 172.24.32.16
        www.rdoproject.org:
          # don't change DNS settings
          playbook_test_mode: True
        # step 1 migration to a MM2 machine
        lists.rdoproject.org:
          # don't change DNS settings
          playbook_test_mode: True
        # future MM3 machine
        future.lists.rdoproject.org:
          ansible_host: 38.145.33.193
          # don't change DNS settings
          playbook_test_mode: True
        blogs.rdoproject.org:
          # don't change DNS settings
          playbook_test_mode: True

    tenant_scl:
      hosts:
        scl-web.osci.io:
          ansible_host: 8.43.85.196

    tenant_spice:
      hosts:
        spice-web.osci.io:
          ansible_host: 8.43.85.198

    tenant_atomic:
      hosts:
        fedora-atomic-1.osci.io:
        fedora-atomic-2.osci.io:
        fedora-atomic-3.osci.io:
        fedora-atomic-4.osci.io:
        fedora-atomic-5.osci.io:
        fedora-atomic-6.osci.io:

    tenant_silverblue:
      hosts:
        proxy.teamsilverblue.org:
          ansible_host: 8.43.85.213

    tenant_patternfly:
      hosts:
        patternfly-forum.osci.io:
          ansible_host: 8.43.85.214
          ansible_python_interpreter: /usr/bin/python3

    # these hosts are not managed here but needed for some deployments
    # (mostly used for delegations)
    unmanaged:
      vars:
        ansible_user: root
      children:
        # asciibinder infra repo not created yet
        tenant_asciibinder:
        #
        tenant_atomic:
        #
        tenant_gdb:
        # oVirt infra on https://gerrit.ovirt.org/infra-ansible
        tenant_ovirt:
        #
        tenant_pcp:
        # Pulp infra on https://github.com/bmbouter/pulp-infra-ansible.git
        # TODO: update link when repo move into the Pulp GH org
        tenant_pulp:
        # SCL infra on https://gitlab.cee.redhat.com/scl/scl.org/
        tenant_scl:
      hosts:
        # listed as unmanaged because ostree wreck havoc with our playbooks
        patternfly-forum.osci.io:

    cage_internal_zone:
      hosts:
        asciibinder-web-builder.int.osci.io:
        osas-community-web-builder.int.osci.io:
        osci-web-builder.int.osci.io:
        ovirt-web-builder.int.osci.io:
        pulp-web-builder.int.osci.io:
        rdo-web-builder.int.osci.io:

    cage_services_zone:
      hosts:
        lucille.srv.osci.io:

    cage_management_zone:
      children:
        supermicro_microblade_sw:

    catatonic:
      children:
        supermicro_microblade_sw:
        supermicro_microblade_blades:
      hosts:
        cmm-catatonic.adm.osci.io:
          ansible_host: 172.24.31.4

    network_equipments:
      children:
        supermicro_microblade_sw:

    not_linux:
      hosts:
        cmm-catatonic.adm.osci.io:
      children:
        supermicro_microblade_sw:

    supermicro_microblade_sw:
      hosts:
        switch-a1-catatonic.adm.osci.io:
          ansible_host: 172.24.31.247
        switch-a2-catatonic.adm.osci.io:
          ansible_host: 172.24.31.248

    supermicro_microblade_blades:
      hosts:
        fedora-atomic-1.osci.io:
        fedora-atomic-2.osci.io:
        fedora-atomic-3.osci.io:
        fedora-atomic-4.osci.io:
        fedora-atomic-5.osci.io:
        fedora-atomic-6.osci.io:
        jerry.osci.io:
        seymour.osci.io:
        lucille.srv.osci.io:
        catton.osci.io:

    virt_hosts:
      hosts:
        speedy.osci.io:
        guido.osci.io:
        jerry.osci.io:
        seymour.osci.io:

    web_builders:
      hosts:
        osas-community-web-builder.int.osci.io:
        rdo-web-builder.int.osci.io:
        osci-web-builder.int.osci.io:

    planet_builders:
      hosts:
        rdo-web-builder.int.osci.io:

    dns_servers:
      children:
        dns_servers_ns1:
          hosts:
            polly.osci.io:
        dns_servers_ns2:
          hosts:
            francine.osci.io:

    mail_servers:
      children:
        mail_servers_mx1:
          hosts:
            polly.osci.io:
        mail_servers_mx2:
          hosts:
            francine.osci.io:

    ml_servers:
      hosts:
        lists.opensourceinfra.org:
        lists.minishift.io:
        lists.rdoproject.org:
        lists.po4a.org:
        lists.nfs-ganesha.org:

    ntp_servers:
      hosts:
        speedy.osci.io:
        guido.osci.io:

    pxe_servers:
      hosts:
        speedy.osci.io:
        www.osci.io:

    sup_servers:
      hosts:
        catton.osci.io:

    # for these hosts services will NOT be restarted automagically after yum/dnf-cron updates
    # this is intended as a stopgap, proper per-service configuration exceptions are prefered
    needrestart_noauto:
      hosts: {}

